from django.db import models

# Create your models here.
class Coach(models.Model):
    name =models.CharField(max_length=20)
    timezone =models.CharField(max_length=50)
    dayofweek = models.CharField(max_length=20)
    available_at = models.TimeField()
    available_to = models.TimeField()

class Slot(models.Model):
    coach = models.ForeignKey(Coach,
        on_delete=models.CASCADE)
    start_time = models.TimeField()
    end_time = models.TimeField()
    status = models.SmallIntegerField(default=1)


