from django.contrib import admin
from django.urls import path
from coach.views import get_coach,list_coach,fetch_slot

urlpatterns = [
    path('list',get_coach,name="list_coach"),
    path('all',list_coach,name="list_coach"),
    path('fetch',fetch_slot,name="fetch_slot"),
]