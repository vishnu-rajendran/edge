from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.decorators import api_view
import pandas as pd
from coach.models import Coach,Slot
import datetime
from datetime import timedelta,date,datetime
import csv
from coach.serializer import CoachSerializer

@api_view(['GET'])
def get_coach(request):
    file_path = "/home/vishnu/Documents/7edge/edge/coaches_schedule.csv"
    data =  Coach.objects.all().delete()
    with open(file_path) as f:
        data = [{k: v for k, v in row.items()} for row in csv.DictReader(f, skipinitialspace=True)]
    for i in data:
        print(i.get('Available at'),i.get('Available until'))
        from_time = datetime.strptime(i['Available at'].strip(), '%H:%M%p').time()
        to_time = datetime.strptime(i['Available until'].strip(), '%H:%M%p').time()
        insert_coach = Coach.objects.create(name=i.get('Name'),timezone =i.get('Timezone') ,dayofweek=i.get('Day of Week') ,available_at =from_time ,available_to =to_time)
        print(insert_coach)
    return Response(data)



@api_view(['GET'])
def list_coach(request):
    data =  Coach.objects.values('id','name','dayofweek').distinct().order_by('name')
    for i in data:
        slots = data_range(i.get('id'))
        i['slots'] = slots
    #serializer_coach = CoachSerializer(data,many=True)
    return Response(data)



def data_range(id):
    data = Coach.objects.get(id=id)
    fr_time = data.available_at
    to_time = data.available_to
    slots = []
    interval = fr_time
    while interval > to_time:
        int_time = datetime.combine(date.today(),interval) + timedelta(minutes=30)
        print(int_time.time())
        interval = int_time.time()
        slots.append(interval)

    data = Slot.objects.filter(coach__id=id).values('end_time')
    for i in data:
        if i.get('end_time') in slots:
            slots.remove(i.get('end_time'))

    return slots


@api_view(['POST'])
def fetch_slot(request):
    data = request.data
    slot = Slot()
    slots = data_range(data.get("id"))
    end_tm = datetime.strptime(data.get("end_time"), '%H:%M').time()
    if end_tm in slots:
        slot.coach_id = data.get("id")
        slot.start_time = data.get("start_time")
        slot.end_time = data.get("end_time")
        slot.save()
        response = {"message": "Successfully Slot Created"}
    else:
        response = {"message": "Slot not available"}
    return Response(response)

